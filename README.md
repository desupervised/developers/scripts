# Scripts

A collection of scripts primarily to be used in docker containers.

For example:

```dockerfile
ADD https://gitlab.com/desupervised/developers/scripts/-/raw/main/wait-for-it.sh /usr/local/bin/wait-for-it
RUN chmod +x /usr/local/bin/wait-for-it
```

NB this will always try to download the script whenever you build. This
invalidates the build cache at that step *and* means you can't build without an
internet connection. To minimise build cache issues, keep these lines near the
end of each Dockerfile. To build without an internet connection replace the URLs
with paths to local copies of the respective scripts.
