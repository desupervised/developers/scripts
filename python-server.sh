#!/bin/bash

# Usage: python-server.sh [PORT]

port=${1:-80}
python -m http.server --directory /srv $port
