#!/bin/bash

[ $# = 0 ] && {
  echo "Usage: $0 COMMAND1 [COMMAND2 [COMMAND3 ...]]"
}

set -e

for arg in "$@"; do
  echo INFO: Executing \'$arg\'...
  eval $arg
done
