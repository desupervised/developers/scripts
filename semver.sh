#!/bin/bash

function latest_version()
{
    VERSION=$(echo $1 $2 | tr ' ' "\n" | sort -V | tail -1)
    echo $VERSION
}

function commit_prefix()
{
    old=$(echo $1 | sed 's/v//')
    new=$(echo $2 | sed 's/v//')
    read old_major old_minor old_patch < <(echo $old | ( IFS=".$IFS" ; read a b c && echo $a $b $c ))
    read new_major new_minor new_patch < <(echo $new | ( IFS=".$IFS" ; read a b c && echo $a $b $c ))

    if [ $new_major \> $old_major ];
    then
        echo "feat!: ";
    elif [ $new_minor \> $old_minor ];
    then
        echo "feat: ";
    else
        echo "fix: ";
    fi;
}
f_call=$1; shift; $f_call "$@"
